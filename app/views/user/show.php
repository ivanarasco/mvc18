<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Detalle de usuario</h1>

      <ul>

        <li> Nombre: <?php echo $user->name ?></li>
        <li> Apellido: <?php echo $user->surname ?></li>
        <li> Edad: <?php echo $user->age ?></li>
        <li> Email: <?php echo $user->email ?></li>

    </ul>

</div>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>


</body>

<?php require "../app/views/parts/scripts.php" ?>
</html>
