<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Vista de usuario</h1>
      <p class="lead">Página de presentación</p>
    </div>

    <div >

      <table class="table table-striped">
        <thead>
          <tr>
            <th> Id </th>
            <th> Nombre </th>
            <th> Apellido </th>
            <th> Edad </th>
            <th> E-mail </th>
            <th> Acciones </th>
          </tr>
        </thead>
        <tbody>
         <?php foreach ($users as $user): ?>
          <tr>
          <td> <?php echo $user->id ?></td>
          <td> <?php echo $user->name ?></td>
          <td> <?php echo $user->surname ?></td>
          <td> <?php echo $user->age ?></td>
          <td> <?php echo $user->email ?></td>
          <td><a href="/user/show/<?php echo $user->id ?>"> Ver </td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>

  </div>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>


</body>

<?php require "../app/views/parts/scripts.php" ?>
</html>
