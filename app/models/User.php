<?php
namespace App\Models;


use PDO;
use Core\Model;

require_once '../core/Model.php';

class User extends Model
{

    function __construct()
    {

    }

    public static function all(){

        $db = User::db(); // creamos la conexión a la base de datos.

        $statement = $db->query('SELECT * FROM users'); // ejecutar consulta SQL para coger todos los usuarios
        $users = $statement->fetchAll(PDO::FETCH_CLASS, User::class); // mete objetos de la clase ( y no de una estándar )
        return $users;
    }

    public static function find($id){

         $db = User::db(); // creamos la conexión a la base de datos.
         $stmt = $db->prepare('SELECT * FROM users WHERE id=:id');
         $stmt->execute(array(':id' => $id));
         $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);
         $user = $stmt->fetch(PDO::FETCH_CLASS);
         //var_dump($user);
         //exit();
         return $user;

}


}
