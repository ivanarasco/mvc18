<?php
namespace App\Controllers;

require_once '../app/models/User.php'; // como el namespace es controllers, nos mira dentro, y ahi no está, está en models.
// esto se pone cuando se quiere "requerir" una clase, solo una vez.

use \App\Models\User;
// esto es para utilizar una clase que está en una ubicación distinta a donde está el namespace declarado (está en Controllers, y User está en models)

class userController
{

    function __construct()
    {
        //echo "<br><br>En UserController!";
    }

    public function index(){

        $users = User::all(); //leer de la base de datos todos los usuarios.
        // :: -> acceso a métodos (estáticos con clase delante)
        require "../app/views/user/index.php";
    }

    public function show($arguments) {

        $id=$arguments[0];
        //var_dump($id);
        //exit();
        $user = User::find($id); // búscame un objeto usuario por su ID.

        require "../app/views/user/show.php";

    }
}
