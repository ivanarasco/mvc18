<?php
//va a ser una clase
namespace core; // paquete

class App
{

    function __construct()
    {
        //echo "Clase App<br>";

        if (isset ($_REQUEST['url'])){
            $url = $_REQUEST['url'];


        }else{
            $url='home';
        }

            // vemos el enlace más allá de mvc18.local ->
            // utilizaremos de este modo las url: controlador/método/argumentos
        $arguments = explode('/', trim($url, '/')); // PARA QUE el / final no afecte.
        $controllerName = array_shift($arguments);
        $controllerName = ucwords($controllerName) . "Controller"; // la primera palabra del array, su primera letra en mayus y le añado Controller (cuando pongas .../user te hará: ../UserController).

        if (count($arguments)) { // esto comprueba si hay algún elemento.
            $method = array_shift($arguments);
        } else {
            $method = "index";
        }


        $file ="../app/controllers/$controllerName" . ".php";
        if (file_exists($file)){
            require_once $file; // para que solo lo haga una vez. EN LLAMADAS A CLASES si se utiliza, cuando son métodos no.
        } else {

            header("HTTP/1.0 404 Not Found");

            echo "No encontrado";
            die(); //termina la ejecución.
        }
        require_once $file;

        $controllerName = '\\App\\Controllers\\' . $controllerName;
        $controllerObject = new $controllerName;

        if (method_exists($controllerName, $method)){

                $controllerObject->$method($arguments);
        } else {

            header("HTTP/1.0 404 Not Found");
            echo "No encontrado";
            die(); //termina la ejecución.
        }





    }

}
